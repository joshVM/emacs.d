(provide 'init-flycheck)
(require 'init-use-package)


(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package flycheck-pos-tip
  :requires flycheck
  :init
  (flycheck-pos-tip-mode))
