(provide 'init-auctex)
(require 'init-company)

;; TODO app-emacs/company-math
;; TODO app-emacs/company-bibtex
;; TODO zotero

;; package for latex
(use-package auctex
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq TeX-save-query nil)
  (setq TeX-PDF-mode t) ;; auto compiling to pdf
  (setq-default TeX-master nil)
  (setq reftex-plug-into-AUCTeX t)
  (add-hook 'LaTeX-mode-hook 'auto-fill-mode)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode)
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  )

;; tex integration for company
(use-package company-auctex
  :requires (tex company)
  :config
  ;; load company integration only if a latex is used.
  (add-hook 'LaTeX-mode-hook
	    (lambda ()
	      (add-to-list (make-local-variable 'company-backends) 'company-auctex)
	      (company-auctex-init)
	      ))
  )
