(provide 'init-use-package)
(require 'package)

;; use-package installs all required packages. use-package is available in
;; melpa, therefore add melpa to the package list

;; update package list with `package-list-packages`

;; load emacs 24's package system. Add MELPA repository.

(setq package-enable-at-startup nil)
(add-to-list   'package-archives
               ;; '("melpa" . "http://stable.melpa.org/packages/")
               '("melpa" . "http://melpa.milkbox.net/packages/"))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

;; always install uninstalled packages
(setq use-package-always-ensure t)
