(provide 'init-python)
(require 'init-company)

(use-package anaconda-mode
  :config
  (add-hook 'python-mode-hook 'anaconda-mode)
  )

(use-package company-anaconda
  :requires (company anaconda-mode)
  :config
  ;; load company integration if anaconda mode is used
  (add-hook 'python-mode-hook
	    (lambda ()
	      (add-to-list (make-local-variable 'company-backends) 'company-anaconda)
	      ))
  )
