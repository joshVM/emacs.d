(provide 'init-company)
(require 'init-use-package)

;; company
(use-package company
  :config
  (add-hook 'after-init-hook 'global-company-mode)
  (setq company-backends '((company-keywords
			    company-files
			    company-capf)
			   (company-abbrev company-dabbrev)
			   ))
  ;; bind tooltip completion to Shift+Tab
  (bind-key* (kbd "<backtab>") 'company-complete)
  ;; show tooltip immediately
  (setq company-idle-delay 0))

(use-package company-quickhelp
  :after (company)
  :config
  (company-quickhelp-mode 1))
