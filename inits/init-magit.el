(provide 'init-magit)

(use-package magit
  :config
  (if (featurep 'init-ivy)
      (setq magit-completing-read-function 'ivy-completing-read)  ;; use ivy
      (setq magit-completing-read-function 'magit-ido-completing-read)))  ;; fallback to default ido

