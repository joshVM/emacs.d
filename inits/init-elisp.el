(provide 'init-elisp)
(require 'init-company)
(require 'init-flycheck)

;; Elisp
;(require company)

(add-hook 'emacs-lisp-mode-hook
	  (lambda ()
	    (add-to-list (make-local-variable 'company-backends) '(company-elisp))
	    ))

(use-package flycheck-cask
  :config
  (add-hook 'flycheck-mode-hook #'flycheck-cask-setup))

