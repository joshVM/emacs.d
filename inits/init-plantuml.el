(provide 'init-plantuml)
(require 'init-use-package)

(use-package plantuml-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode)))

(use-package flycheck-plantuml
  :config
  (flycheck-plantuml-setup))



(defun plantuml-save-to-file ()
  "Save the generated image to a file.
This command will as for the path and filename to which the image
should be saved. 
This command should be executed from the *PLANTUML Preview* buffer only"
  (interactive)
  (clipboard-kill-ring-save (point-min) (point-max))
  (generate-new-buffer "untitled")
  (switch-to-buffer "untitled")
  (yank)
  (save-buffer)
)
