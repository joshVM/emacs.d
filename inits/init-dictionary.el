(provide 'init-dictionary)
(require 'init-company)
(require 'init-flycheck)

;; flyspell-mode

(defun german()
  (interactive)
  (ispell-change-dictionary "deutsch")
  (flyspell-mode 1))

(defun english()
  (interactive)
  (ispell-change-dictionary "english")
  (flyspell-mode 1))

(add-hook 'flyspell-mode-hook
	  (lambda ()
	    (add-to-list (make-local-variable 'company-backends) '(company-ispell))
	    (ispell-change-dictionary "english")))

