(provide 'init-projectile)

;; projectile
(use-package projectile
  :config
  (setq projectile-enable-caching t)

  ;; support for ivy
  (if (featurep 'init-ivy)
      (setq projectile-completion-system 'ivy))

  ;; open dired, when switching project
  (setq projectile-switch-project-action #'projectile-dired)
  (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1))
