(provide 'init-ivy)
(require 'init-use-package)

;; this is a replacement for the default ido search engine.
;; don't use at the same time as init-ido


(use-package counsel
  :init
  (counsel-mode 1)
  :demand
  :config
  (global-set-key "\C-s" 'swiper)
  (setq ivy-height 20)
  (setq ivy-display-style 'fancy)
  (setq enable-recursive-minibuffers t)
  (setq ivy-use-virtual-buffers t
	ivy-count-format "%d/%d ")
  (setq ivy-re-builders-alist '((t . ivy--regex-fuzzy))))
