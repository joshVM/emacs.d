(provide 'init-bash)
(require 'init-flycheck)

(use-package flycheck-checkbashisms
  :requires flycheck
  :ensure t
  :config
  (flycheck-checkbashisms-setup))

