(provide 'init-web)
(require 'init-company)

(use-package web-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.html$" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
  )

;; web-mode integration for company
(use-package company-web
  :requires (web-mode company)
  :config
  ;; only load company integration if web-mode is used
  (add-hook 'web-mode-hook
	    (lambda ()
	      (add-to-list (make-local-variable 'company-backends) '(company-web-html))
	      ))
  )
