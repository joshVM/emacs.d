(provide 'init-theme)
(require 'init-use-package)

;; theming small color fix for terminals (works with emacs server, too). It
;; disables of emacs background color and uses the colors of the
;; terminal. Therfore it is possible to switch the terminal colors and emacs's
;; background changes too.
;; (defun on-frame-open (frame)
;;   (unless (display-graphic-p frame)
;;     (set-face-background 'default "unspecified-bg" frame)
;;     (set-face-foreground 'default "0" frame) ; some errors are printed, but it works
;;     ))
;; (on-frame-open (selected-frame))
;; (add-hook 'after-make-frame-functions 'on-frame-open)


;; colorice nested brackets
(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

;; bottom bar
;; respectful theme chooses colors depending on load-theme.
(use-package smart-mode-line
  :config
  (setq sml/no-confirm-load-theme t)
  (setq sml/theme 'respectful)
  (sml/setup))


(use-package doom-themes
  :init
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled  
  ;; Load the theme (doom-one, doom-molokai, etc); keep in mind that each theme
  ;; may have their own settings.
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  (doom-themes-neotree-config)
  ;; or for treemacs users
;  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
;  (doom-themes-org-config)
  ;; Shortcut to toggle between light and dark theme
  (setq dark-or-light 'dark)
  (defun toggle-theme ()
    (interactive)
    (if (eq dark-or-light 'light)
        ;; load dark theme
        (progn
          (setq dark-or-light 'dark)
          (load-theme 'doom-one t))
      ;; load light theme
	  (progn
        (setq dark-or-light 'light)
        (load-theme 'doom-one-light t))
      )
    )
  ()
  :config
  (load-theme 'doom-one t)
  :bind (("C-c c" . toggle-theme)))


;; (use-package minimap
;;   :config
;;   (custom-set-variables
;;    '(minimap-update-delay 0.02)
;;    '(minimap-window-location (quote right)))

;;   (custom-set-faces
;;    '(minimap-active-region-background ((t (:inverse-video t))))
;;    '(minimap-update-delay 0.02)
;;    '(minimap-window-location (quote right)))
  
;;   (minimap-mode 1)
;;   )

(use-package dimmer
  :config
  (dimmer-mode 1))
