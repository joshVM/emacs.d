(provide 'init-irony)
(require 'init-company)
(require 'init-flycheck)

;; M-x irony-install-server
;; deps general:
;; * CMake >= 2.8.3
;; * libclang
;; arch-linux: `pacman -S clang cmake`

(use-package irony
  :config
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

  (add-hook 'irony-mode
	    (lambda ()
	      (add-to-list (make-local-variable 'company-backends) '(company-gtags))
	      )))


;; TODO: irony-mode hook , after: irony
(use-package company-irony
  :requires (company irony)
  :init
  (add-hook 'irony-mode
	    (lambda ()
	      (add-to-list (make-local-variable 'company-backends)
	                   '(company-irony)) )) )

;; TODO: irony-mode hook , after: irony
(use-package company-irony-c-headers
  :requires (company irony)
  :init
  (add-hook 'irony-mode
	    (lambda ()
	      (add-to-list (make-local-variable 'company-backends) '(company-irony-c-headers))
	      )))

(use-package flycheck-irony
  :requires (flycheck irony)
  :init
  (add-hook 'flycheck-mode-hook #'flycheck-irony-setup))

