;;; package --- Summary
;;; Commentary:
;;; Code:
(require 'init-use-package)

;; fuzzy matching for ido (ido is the default search engine in emacs)


(ido-mode 1)

;; support editing files with sudo
(defadvice ido-find-file (after find-file-sudo activate)
  "Find file as root if necessary."
  (unless (and buffer-file-name
               (file-writable-p buffer-file-name))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))
 
(use-package flx-ido
  :config
  (flx-ido-mode 1)
  (ido-everywhere 1)
  (setq ido-enable-flex-matching t))
  

;; use ido everywhere
(use-package ido-completing-read+
  :after (flx-ido)
  :config
;  (setq gc-cons-threshold 20000000) ;;; tune garbage collection of emacs for speed up.
  (ido-ubiquitous-mode 1))


(provide 'init-flx-ido)
;;; init-flx-ido.el ends here
