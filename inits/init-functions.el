(provide 'init-functions)

;; date function
(defun insert-date (prefix)
  "Insert the current date. With prefix-argument, use ISO format. With
   two prefix arguments, write out the day and month name."
  (interactive "P")
  (let ((format (cond
		 ((not prefix) "%Y-%m-%d")))
	(system-time-locale "de_DE"))
    (insert (format-time-string format))))
(global-set-key (kbd "C-c d") 'insert-date)

;; Shortcut to toggle between light and dark theme
(global-set-key (kbd "C-c c") 'toggle-theme)
(setq dark-or-light 'dark)
(defun toggle-theme ()
  (interactive)
  (if (eq dark-or-light 'light)
      ;; load dark theme
      (progn
        (setq dark-or-light 'dark)
        (load-theme 'doom-one t))
    ;; load light theme
	(progn
      (setq dark-or-light 'light)
      (load-theme 'doom-one-light t))
    )
  )



