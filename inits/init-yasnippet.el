(provide 'init-yasnippet)
(require 'init-use-package)
(require 'init-company)


(use-package yasnippet
  :config
  (yas-global-mode 1)
  (add-to-list (make-local-variable 'company-backends) '(company-yasnippet)))

(use-package yasnippet-snippets
  :requires yasnippet)
