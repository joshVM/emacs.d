 (provide 'init-core)

;; basic configuration
(menu-bar-mode -1)

;; the tool-bar and scroll-bar are disabled. But both functions only exist, if
;; emacs is compiled with GUI support. This is not the case for emacs-nox. In
;; order to support emacs server, these variables are set for every new window.
(defun new-frame-setup (frame)
  (if (display-graphic-p frame)
      (progn
	(message "window system")
	(tool-bar-mode -1)
	(scroll-bar-mode -1))
    (message "not a window system")))

;; Run for already-existing frames
(mapc 'new-frame-setup (frame-list))
;; Run when a new frame is created
(add-hook 'after-make-frame-functions 'new-frame-setup)


(setq scroll-step 1)

;; no startup msg  
(setq inhibit-startup-message t)

;; Makes *scratch* empty.
(setq initial-scratch-message "")

;; Don't show *Buffer list* when opening multiple files at the same time.
(setq inhibit-startup-buffer-menu t)

;;Show only one active window when opening multiple files at the same time.
(add-hook 'window-setup-hook 'delete-other-windows)

;; disable backup and lockfiles
;#(setq backup-inhibited t)
;(setq create-lockfiles nil) 
(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files

;; font size
(set-face-attribute 'default nil :height 80)

;; enforce 80-character rule
(add-hook 'text-mode-hook 'auto-fill-mode) ; or press M-q for manual reformating
(setq-default fill-column 80)

;; create a second bracket
(electric-pair-mode 1)
;; highlight the matching bracket
(show-paren-mode 1)
(set-face-foreground 'show-paren-match "#def")
(set-face-background 'show-paren-match (face-background 'default))
(set-face-attribute 'show-paren-match nil :weight 'extra-bold)
(setq show-paren-delay 0)


;; linum-mode
(if (version<= emacs-version "26.0.50")
    (global-linum-mode t)
  (global-display-line-numbers-mode))

;; use both clipboards
(setq x-select-enable-clipboard t)
(setq x-select-enable-primary t)

;; easypg
(require 'epa-file)
(epa-file-enable)

;; text-mode
(setq default-major-mode 'text-mode)
