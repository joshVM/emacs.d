# Supported Emacs Versions
* Emacs 25
* Emacs 26

# Tested Distributionen
* Arch Linux (Emacs 26.1)
* Ubuntu 18.04 (Emacs 25.2.2)
* Ubuntu 16.04 (Emacs 25.3.2)
* Ubuntu 19.04 (Emacs 26.1)
* Ubuntu 19.10 (Emacs 26.1)

# Installation
## Arch Linux
Install Emacs in [Arch-Linux](https://wiki.archlinux.org/index.php/Emacs):
```bash
sudo pacman -S emacs

# run emacs without GUI when started in terminal
echo 'export EDITOR="emacs -nw"' >> ~/.bashrc
echo 'alias emacs="emacs -nw"' >> ~/.bashrc
```
If you want to use emacs server:
```bash
# replace the previously defined lines with
echo 'alias emacs.standalone="emacs -nw"' >> ~/.bashrc
echo 'alias emacs="emacsclient -t"' >> ~/.bashrc
echo 'export EDITOR="emacsclient -t"' >> ~/.bashrc

# start emacs without server in order to install all packages
emacs.standalone

# enable and start emacs server
systemctl --user enable --now emacs

```
Running `emacs` will start an emacs client, running `emacs.standalone` will
start a emacs instance without server connection. In order to reload emacs server:
``` bash
systemctl --user restart emacs.service
```

## Ubuntu 18.04
Install Emacs in [Ubuntu 18.04](https://wiki.ubuntuusers.de/Emacs/):

``` bash
# install emacs with GUI support
sudo apt-get install emacs

# install emacs without GUI support
sudo apt-get install emacs-nox
```

## Ubuntu 16.04
Ubuntu 16.04 only has Emacs 24, which is not supported by this configration. You
can [compile Emacs 25](https://www.debyum.com/install-emacs-in-ubuntu/) or use
this PPA:

``` bash
sudo add-apt-repository ppa:kelleyk/emacs
sudo apt update

# install emacs with GUI support
sudo apt install emacs25

# install emacs without GUI support
sudo apt-get install emacs25-nox
```

# Configuration
```bash
# backup old configuration
mv ~/.emacs.d ~/.emacs.d.old

# download new emacs configuration
git clone https://gitlab.com/apero/emacs.d.git ~/.emacs.d
cd ~/.emacs.d/
cp init.el.example init.el
```

Open `~/.emacs.d/init.el` and comment packages with `;;` which should be
disabled.


# Problems
* X11 Passthrough with XWayland has graphical glitch
